﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using NTier_WebAPI.Modelss;
using System.Web.Security;

namespace NTier_WebAPI.Controllers
{
    public class LoginAccountController : Controller
    {
        //Registration Action
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "IsEmailVerified,ActivationCode")] user user)
        {
            bool Status = false;
            string message = "";
            if (ModelState.IsValid)
            {
                var isExist = IsEmailExist(user.email);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist");
                    return View(user);                    
                }
                //Generate Activation Code
                user.ActivationCode = Guid.NewGuid();
                //Password Hashing
                user.password = Crypto.Hash(user.password);
                user.ConfirmPassword = Crypto.Hash(user.ConfirmPassword);
                user.IsEmailVerified = false;
                //Save to Database
                using(testCRUDEntities dc = new testCRUDEntities())
                {
                    dc.users.Add(user);
                    dc.SaveChanges();
                    //Send Email to User
                    SendVerificationLinkEmail(user.email, user.ActivationCode.ToString());
                    message = "Đăng ký thành công! Link kích hoạt tài khoản " +
                        " đã được gửi đến địa chỉ email:" + user.email;
                    Status = true;
                }
            }
            else
            {
                message = "Không hợp lệ!";
            }
            ViewBag.Message = message;
            ViewBag.Status = Status;
            return View(user);
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            using (testCRUDEntities dc = new testCRUDEntities())
            {
                dc.Configuration.ValidateOnSaveEnabled = false;
                var v = dc.users.Where(a => a.ActivationCode == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.IsEmailVerified = true;
                    dc.SaveChanges();
                    Status = true;
                }
                else
                {
                    ViewBag.Message = "Không hợp lệ!";
                }
            }
            ViewBag.Status = Status;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin login, string ReturnUrl)
        {
            string message = "";
            using(testCRUDEntities dc = new testCRUDEntities())
            {
                var v = dc.users.Where(a => a.email == login.email).FirstOrDefault();
                if(v!= null)
                {
                    if (string.Compare(Crypto.Hash(login.password), v.password) == 0)
                    {
                        int timeout = login.RememberMe ? 525000 : 20;
                        var ticket = new FormsAuthenticationTicket(login.email, login.RememberMe, timeout);
                        string encrypted = FormsAuthentication.Encrypt(ticket);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                        cookie.Expires = DateTime.Now.AddMinutes(timeout);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);

                        if (Url.IsLocalUrl(ReturnUrl))
                        {
                            return Redirect(ReturnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "mvcusers");
                        }
                    }
                    else
                    {
                        message = "Thông tin không hợp lệ!";
                    }

                }
                else
                {
                    message = "Thông tin không hợp lệ!";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "LoginAccount");
        }
        [NonAction]
        public bool IsEmailExist(string email)
        {
            using(testCRUDEntities dc = new testCRUDEntities())
            {
                var v = dc.users.Where(a => a.email == email).FirstOrDefault();
                return v != null;
            }
        }
        [NonAction]       
        public void SendVerificationLinkEmail(string email, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/LoginAccount/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("mariobolobala@gmail.com", "mario bolobala");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "tranhuutich1";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Tài khoản của bạn đã được tạo. Vui lòng kích hoạt tại link kèm theo!";
                body = "<br/><br/>Chúng tôi vui lòng thông báo tài khoản của bạn" +
                    " đã được tạo thành công. Vui lòng click vào link bên dưới để xác nhận" +
                    " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "<br/><br/>Chúng tôi nhận được thông báo yêu cầu cấp lại mật khẩu. Vui lòng click vào đường lên bên dưới để tiến hành tạo lại mật khẩu mới." +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            string message = "";
            bool status = false;
            using (testCRUDEntities dc = new testCRUDEntities())
            {
                var account = dc.users.Where(a => a.email == email).FirstOrDefault();
                if (account != null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.email, resetCode, "ResetPassword");
                    account.ResetPasswordCode = resetCode;
                    dc.Configuration.ValidateOnSaveEnabled = false;
                    dc.SaveChanges();
                   
                    
                }
                else
                {
                    message = "Không tìm thấy tài khoản trùng khớp!!!";
                }
            }
            return View();
        }

        public ActionResult ResetPassword(string id)
        {
            using (testCRUDEntities dc = new testCRUDEntities())
            {
                var user = dc.users.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                if(user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using(testCRUDEntities dc = new testCRUDEntities())
                {
                    var user = dc.users.Where(a => a.ResetPasswordCode == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.password = Crypto.Hash(model.NewPassword);
                        user.ResetPasswordCode = "";
                        dc.Configuration.ValidateOnSaveEnabled = false;
                        dc.SaveChanges();
                        message = "Mật khẩu đã được tạo mới!";
                    }
                }
            }
            else
            {
                message = "Không hợp lệ!";
            }
            ViewBag.Message = message;
            return View(model);
        }
    }
}