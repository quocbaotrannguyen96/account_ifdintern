﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTier_WebAPI.Modelss;
using AutoMapper;
using System.Transactions;

namespace NTier_WebAPI.BusinessLogic
{
    public class UserManagementService : IUserManagementService
    {
        private readonly UnitOfWork _unitOfWork;
        public UserManagementService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public user GetUserById(int id)
        {
            return _unitOfWork.IUserRepository.GetByID(id);
            
        }
        public IEnumerable<user> GetAllUsers()
        {
            return _unitOfWork.IUserRepository.GetAll().ToList();
            
        }
        public int CreateUser(user userEntity)
        {           
            _unitOfWork.IUserRepository.Insert(userEntity);
            _unitOfWork.Save();
            return userEntity.id;
           
        }
        public bool UpdateUser(int id, user userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.IUserRepository.GetByID(id);                    
                    if (usr != null)
                    {
                        usr.username = userEntity.username;
                        _unitOfWork.IUserRepository.Update(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool DeleteUser(int id)
        {
            var success = false;
            if (id > 0)
            {
                using (var scope = new TransactionScope())
                {  
                    var usr = _unitOfWork.IUserRepository.GetByID(id);
                    if (usr != null)
                    {

                        _unitOfWork.IUserRepository.Delete(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
                
    }
}
