﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTier_WebAPI.Modelss
{
    [MetadataType(typeof(UserMetadata))]
    public partial class user
    {
        public string ConfirmPassword { get; set; }
    }

    public class UserMetadata
    {
        [Display(Name = "Họ và Tên")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string username { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name = "Mật khẩu")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Tối thiểu 6 ký tự!")]
        public string password { get; set; }

        [Display(Name = "Xác nhận mật khẩu")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "Không trùng khớp!")]
        public string ConfirmPassword { get; set; }

    }
}